import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AuthentificationProvider } from "../../providers/authentification/authentification";
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
param1: string;
param2:string;
private allParams;
  
constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthentificationProvider) {
    this.param1 = this.navParams.get("param1");
    this.param2 = this.navParams.get("param2");
    this.allParams = this.navParams.data ;

    console.log(this.allParams);
    
  }

  login() {
      
    this.auth.signOut();
    this.navCtrl.setRoot(LoginPage);
  }

  logout() {
    
    this.auth.signOut();
    this.navCtrl.push(AboutPage);
  }

}
