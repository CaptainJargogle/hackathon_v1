import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { AuthentificationProvider } from "../../providers/authentification/authentification";
import { auth } from 'firebase';

import { SignupPage } from "../signup/signup";
import { HomePage } from "../home/home";





@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class LoginPage {
	loginForm: FormGroup;
	loginError: string;
  

	constructor(
		private loadingCtrl: LoadingController,
		private alertCtrl: AlertController,
		private navCtrl: NavController,
		private auth: AuthentificationProvider,
		fb: FormBuilder
	) {
		this.loginForm = fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
		});
	}

  login() {
		let data = this.loginForm.value;

		if (!data.email) {
			return;
		}

		let credentials = {
			email: data.email,
			password: data.password
		};
		this.auth.signInWithEmail(credentials)
			.then(
        () => {this.auth.enabled=true; this.navCtrl.setRoot(HomePage)},
        
				error => this.loginError = error.message
			);
}


signup(){
  this.navCtrl.push(SignupPage);
}

forgotPassword(){
	let prompt = this.alertCtrl.create({
	title: "Enter your Email",
	message: "a new password will be sent to you",
	inputs: [
		{
			name: "recoverEmail",
			placeholder: "you@example.com"
		}],
		buttons: [
			{
				text: "Cancel",
				handler: data => {

				}
			},
			{
				text: "Submit",
				handler: data => {
					// add Placeholder.
				let loading = this.loadingCtrl.create({
					dismissOnPageChange: true,
					content: "Reseting your Password.."	
				});	
					
					loading.present();
			
					
					//calling AuthService Provider
		this.auth.forgotPasswordUser(data.recoverEmail).then(() =>{
			//add toast
			loading.dismiss().then(() => {
				//show pop up
				let alert = this.alertCtrl.create({
					title: "Check your Email",
					subTitle: "Password reset successful!",
					buttons: ["OK"]
				
				});
				alert.present();

			})
		},
			error => {
				
				//show pop up
				loading.dismiss().then(() => {
				let alert = this.alertCtrl.create({
					title: "Error resetting password",
					subTitle: error.message,
					buttons: ["OK"]
				});
				alert.present();
			})
				
				});
		
			}}]});
			prompt.present();




		}
}