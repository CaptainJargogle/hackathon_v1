import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage} from "../login/login";
import { AuthentificationProvider } from "../../providers/authentification/authentification";
import { auth } from 'firebase';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  enabled: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthentificationProvider) {
    this.enabled= auth.enabled;

  }

  goToAbout(){
    this.navCtrl.push(LoginPage,{param1 : "hello" , param2 : "world"});

  }


   
    login() {
      
      this.auth.signOut();
      this.navCtrl.setRoot(LoginPage);
    }

    logout() {
      
      this.auth.signOut();
      this.navCtrl.setRoot(HomePage);
    }
    
  }

  


