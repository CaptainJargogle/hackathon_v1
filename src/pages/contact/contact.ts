import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { AuthentificationProvider } from "../../providers/authentification/authentification";
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController, public auth: AuthentificationProvider) {

  }
  login() {
      
    this.auth.signOut();
    this.navCtrl.setRoot(LoginPage);
    
  }

  logout() {
    
    this.auth.signOut();
    this.navCtrl.setRoot(ContactPage);
  }

}
