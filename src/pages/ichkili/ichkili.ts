import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, } from 'ionic-angular';
import { AuthentificationProvider } from "../../providers/authentification/authentification";

import { LoginPage } from "../login/login";



import { File } from '@ionic-native/file/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MediaCapture,CaptureError, CaptureVideoOptions, MediaFile } from '@ionic-native/media-capture';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the IchkiliPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

const MEDIA_FILES_KEY = "mediaFiles";
@Component({
  selector: 'page-ichkili',
  templateUrl: 'ichkili.html',
})
export class IchkiliPage {
  @ViewChild("myvideo") myVideo: any;
  mediaFiles = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthentificationProvider,
    private mediaCapture: MediaCapture,
    private storage: Storage,
    private media: Media,
    public file: File,
    private alertCtrl: AlertController,

  ) {
  }

  ionViewDidLoad() {
    this.storage.get(MEDIA_FILES_KEY).then(res => {
      this.mediaFiles = JSON.parse(res) || [];
    })
  }
 
  captureAudio() {
    this.mediaCapture.captureAudio().then(res => {
      this.storeMediaFiles(res);
    }, (err: CaptureError) => console.error(err));
  }
 
  captureVideo() {
    let options: CaptureVideoOptions = {
      limit: 1,
      duration: 30
    }
    this.mediaCapture.captureVideo(options).then((res: MediaFile[]) => {
      let capturedFile = res[0];
      let fileName = capturedFile.name;
      let dir = capturedFile['localURL'].split('/');
      dir.pop();
      let fromDirectory = dir.join('/');      
      var toDirectory = this.file.dataDirectory;
      
      this.file.copyFile(fromDirectory , fileName , toDirectory , fileName).then((res) => {
        this.storeMediaFiles([{name: fileName, size: capturedFile.size}]);
      },err => {
        console.log('err: ', err);
      });
          },
    (err: CaptureError) => console.error(err));
  }
 
  // play(myFile) {
  //   if (myFile.name.indexOf('.wav') > -1) {
  //     const audioFile: MediaObject = this.media.create(myFile.localURL);
  //     audioFile.play();
  //   } else {
  //     let path = this.file.dataDirectory + myFile.name;
  //     let url = path.replace(/^file:\/\//, '');
  //     let video = this.myVideo.nativeElement;
  //     video.src = url;
  //     video.play();
  //   }
  // }
 
  storeMediaFiles(files) {
    this.storage.get(MEDIA_FILES_KEY).then(res => {
      if (res) {
        let arr = JSON.parse(res);
        arr = arr.concat(files);
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(arr));
      } else {
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(files))
      }
      this.mediaFiles = this.mediaFiles.concat(files);
    })
  }
  login() {

    this.auth.signOut();
    this.navCtrl.setRoot(LoginPage);


  }

  logout() {

    this.auth.signOut();
    this.navCtrl.setRoot(IchkiliPage);
  }



}
