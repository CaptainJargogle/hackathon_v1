import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;


@Injectable()
export class AuthentificationProvider {

  private user: firebase.User;
  public enabled =false;
  

	constructor(public afAuth: AngularFireAuth) {
		afAuth.authState.subscribe(user => {
			this.user = user;
		});
	}

	signInWithEmail(credentials) {
		console.log('Sign in with email');
		return this.afAuth.auth.signInWithEmailAndPassword(credentials.email,
			 credentials.password);
}

signUp(credentials) {
	return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
}

signOut(): Promise<void> {
	
	this.enabled=false;
  return this.afAuth.auth.signOut();
  
}

get authenticated(): boolean {
  return this.user !== null;
}

getEmail() {
  return this.user && this.user.email;
}

  // sendPasswordResetEmail() is a funvtion provided by the firebase SDK.
	forgotPasswordUser( email : any){
		return this.afAuth.auth.sendPasswordResetEmail(email);
	}


}